# Theory of Everything

## Purpose
This is my model explaining my current understandings of all fundamental physics

## Contributing

### Issues
Anyone may create issues. The issues that will be given priority are ones which cite (preferably) relevant and credible resources (theories, freely-accessible papers, experiments, studies, etc):
- 3 or more which overall **support** the issue
- 3 or more which overall **oppose** the issue

### Donations
Currently, there is no way to donate money set up, but if this is requested in an **issue**, it will be considered.

### Appreciations
In general, growing up and on an ongoing basis, I learn a lot from, and appreciate
- Anton Petrov (WhatDaMath)
- Becky Smethurst (Dr. Becky)
- Bill Nye (The Science Guy!)
- Brian Greene
- Carl Sagan
- Derek Muller (Veritasium)
- Destin Sandlin (SmarterEveryDay)
- Henry Reich (Minutephysics)
- Matt O'Dowd (PBS Space Time)
- Neil DeGrasse Tyson (StarTalk)
- Philipp Dettmer & Steve Taylor (Kurzgesagt)
- Sabine Hossenfelder (Science without the gobbledygook)
- Stephen Hawking
- And definitely more who I'm just not remembering at the moment, too

## Current questions
- Is this model compatible with GR and/or QM? If not, why not?
  - Does it explain gravity?
  - Does it explain fundamental forces?
- Does it explain how "dimensions" of space emerge? Or is each "dimension"/"coordinate" just a different property+value within particles?
- Does it explain currently unsolved mysteries (black holes, hubble tension, dark matter, dark energy, casimir effect, etc)
