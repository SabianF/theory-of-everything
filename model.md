*(This is the model all in one text block in order of most fundamental to most emergent. I may or may not reorganize this later)*

## Energy
WIP: What is energy?

*(I currently lean towards believing that the "wave function" is a mathematical approximation of more fundamental "hidden variables", but solving this requires overcoming the "measurement problem")*

*(I want to do more research on QM before declaring a definition for this)*

*(I currently have no clue how to define energy; it may be the single fundamental propery of the universe from which all else emerges)*

## Particles
WIP: What is a particle?

*(I currently lean towards believing that all elementary particles are massless and that any particle with mass is actually composed of massless elementary particles, but in the current Standard Model of particle physics, some particles defined with mass seem to be elementary)*

*(I want to do more research on QM before declaring a definition for this)*

*(Idea: particles are contained energy; but how can energy be "contained" in space to form particles if space itself emerges between particles?)*

## Space
Space is relative: it emerges from the "distance between" (the difference of value for a certain property of) particles, where
- Space exists "between" particles
- There is no space outside the "connecting lines" (like a neural network) of the particles
- If there are no connecting lines, then there is no space. Thus, if there are fewer than 2 particles, then there is no space, meaning if there is only one particle in "no space", then there is seemingly infinite energy density (aka a "singularity")

## Time
Time is cyclic motion: it emerges from the repeating movement of particles. It is not a "dimension", but is an accumulated value whose rate of accumulation can only be 0 or positive, and the "passage of time" is simply the "physics of sequential interactions of particles".
- If there are no cycles, then there is no time. Thus, if there are no particles, then there is no time.
- If there are fewer cycles, then "time slows down" (both aging and the perception of time of the observer slows, since aging is a compound effect of interactions, and perception is a computational result of interactions)

Think of the cesium atomic clock which measures frequent oscillations (cycles) happening within the cesium atom to "measure" the passage of time in a consistent way.

Also think of photons seeming to not experience time, because their motion (at least at our scales of measurement) is not cyclic, but linear.

Many people believe that everything will stop if time freezes, but in this model, it's time that freezes when everything stops.

This model refutes the idea of "if one end of a wormhole is accelerated, then it would be a time machine", because that idea presumes time is a dimension and that the two wormhole ends would move into different "positions" or "locations" of time, causing a traveller to "go into the past/future". But in this model, acceleration simply slows the "aging" (reduces the cycling frequency) of one wormhole end, resulting in the two ends simply being different "ages" without any "time travel" happening between them.

## Mass
Mass is confined particles: it emerges from particles that are bound (contained) within a localized region of space.

Think of how protons and neutrons are made up of 3 quarks each, but the total mass of the quarks (given to them by the Higgs Field) is far less than the total mass of these protons and neutrons. The majority of the mass of baryons (and perhaps mesons) actually comes from what many people call the "binding energy of the gluon field", which in this model is the "confinement" of these particles.

## Forces
TODO

### Strong
TODO

### Weak
TODO

### Electromagnetism
TODO

### Gravity
TODO
